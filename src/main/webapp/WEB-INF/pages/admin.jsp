<%@include file="/WEB-INF/include/page.jsp" %>

<!doctype html>
<html>
<head>
    <title>Quotes</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="quotes">
        <c:choose>
            <c:when test="${empty requestScope.list}">
                Nothing to show
            </c:when>
            <c:when test="${not empty requestScope.list}">
            <table id="usertable">
            	<tr><td>Username</td><td>block / unblock</td></tr>
                <c:forEach var="user" items="${requestScope.list}">
                	<tr>
                		<td><c:out value="${user.name}"/></td>
		                <td>
			            			
				               		<input type="checkbox" name="userid" value="${user.id}" <c:if test="${user.blocked}"> checked </c:if> />
			             	
            			</td>
                	</tr>
                 </c:forEach>
            </table>
            </c:when>
        </c:choose>
    </div>
    <c:if test="${not empty requestScope.prevPage}">
        <form:form class="left" method="get">
            <input type="hidden" value="${requestScope.prevPage}" name="page">
            <input type="submit" value="<spring:message code="quotes.previous"/>">
        </form:form>
    </c:if>
    <c:if test="${not empty requestScope.nextPage}">
        <form:form class="right" method="get">
            <input type="hidden" value="${requestScope.nextPage}" name="page">
            <input type="submit" value="<spring:message code="quotes.next"/>">
        </form:form>
    </c:if>
</div>
<script>var ctx = "${pageContext.request.contextPath}";</script>
<script>var csrf = "${_csrf.token}";</script>
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
</body>
</html>