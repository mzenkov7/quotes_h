<%@include file="/WEB-INF/include/page.jsp" %>

<div id="header">
    <div class="right">
        <%@include file="/WEB-INF/include/lang.jsp" %>
        <sec:authorize access="isAuthenticated()">
            <form:form method="post" action="${ctx}/logout">
                <c:out value="${sessionScope.user.name} /"/>
                <input type="submit" value="<spring:message code="logout.text"/>"/>
            </form:form>
        </sec:authorize>
        <sec:authorize access="isAnonymous()">
            <a href="<c:url value="/login"/>">Login</a> /
            <a href="<c:url value="/register"/>">Register</a>
        </sec:authorize>
                
    </div>

    <ul id="nav">
        <li>
            <a href="<c:url value="/"/>"><spring:message code="quotes.new"/></a>
        </li>
        <li>
            <a href="<c:url value="/random"/>"><spring:message code="quotes.random"/></a>
        </li>
        <li>
            <a href="<c:url value="/best"/>"><spring:message code="quotes.best"/></a>
        </li>
        <li>
            <a href="<c:url value="/quote/add"/>"><spring:message code="quotes.add"/></a>
        </li>
        
        <sec:authorize access="hasRole('ROLE_ADMIN')">
	        <li>
	            <a href="<c:url value="/admin"/>"><spring:message code="adminsection"/></a>
	        </li>
        </sec:authorize>
        
        <li>
            <a href="<c:url value="/search"/>"><spring:message code="quotes.search"/></a>
        </li>
    </ul>
    <div style="clear: both"></div>
</div>