$('.rating-up').click(function() {
	var $idElem = $(this).siblings("input[type=hidden]");
	changeRating($idElem, +1);
});

$('.rating-down').click(function() {
	var $idElem = $(this).siblings("input[type=hidden]");
	changeRating($idElem, -1);
});

$('#usertable input[type="checkbox"]').change(function() {

	var chk = $(this).attr("value");

	var chkVal = $(this).is(':checked');

	$.ajax({
		type : 'POST',
		url : ctx + '/admin/ban',
		headers : {
			'X-CSRF-TOKEN' : csrf
		},
		data : {
			name : chk,
			value : chkVal
		},
		dataType : 'json',
		// beforeSend: function() { alert(chk + ' ' + chkVal); },
		success : function(data, textStatus, jqXHR) {
			alert(data.user + ' ' + data.blocked);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert('Error occurred: \n' + errorThrown);
		}

	});

});

function changeRating($idElem, $value) {

	var $id = $idElem.val();
	var $ratingElem = $idElem.siblings(".rating");
	var $incElem = $idElem.siblings(".rating-up");
	var $decElem = $idElem.siblings(".rating-down");

	var data = {
		'quote-id' : $id,
		'value' : $value
	};

	$.ajax({
		type : 'POST',
		url : ctx + '/quote/rating',
		headers : {
			'X-CSRF-TOKEN' : csrf
		},
		data : data,
		dataType : 'json',
		success : function(data, textStatus, jqXHR) {
			$ratingElem.text(data.newRating);
			$incElem.remove();
			$decElem.remove();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert('Error occurred: \n' + errorThrown);
		}
	});
}
