package ua.karghoff.quotes_h.entity;

/**
 * Representation of Database user role entity.
 */
public enum Role {
    ROLE_ADMIN, ROLE_USER
}
