package ua.karghoff.quotes_h.service;

import ua.karghoff.quotes_h.entity.BasicEntity;

import java.util.List;

/**
 * Service, which provide sorting and pagination methods.
 *
 * @param <T> Entity type
 */
public interface SortedService<T extends BasicEntity> extends Service<T> {

    List<T> getSorted(String columnName, boolean reverse, int offset, int limit);
}
