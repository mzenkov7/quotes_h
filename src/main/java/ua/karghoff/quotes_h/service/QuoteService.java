package ua.karghoff.quotes_h.service;

import org.springframework.data.domain.Page;
import ua.karghoff.quotes_h.entity.Quote;

import java.util.List;

/**
 * Service for {@link Quote} entity.
 */
public interface QuoteService extends SortedService<Quote> {

    /**
     * Change rating for given Quote.
     *
     * @param quote quote to rating change
     * @param diff  difference between current and new rating
     */
    void changeRating(Quote quote, int diff);

    List<Quote> getRandomQuotes(long count);

    Page<Quote> findByText(String searchQueryText, int offset, int limit);
}
