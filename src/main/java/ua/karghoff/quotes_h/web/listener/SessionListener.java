package ua.karghoff.quotes_h.web.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;

/**
 * Listener to provide session user votes storage.
 */
public class SessionListener implements HttpSessionListener {

    /**
     * Add user votes map to user session. <br>
     * Keep quotes for which the user voted.
     *
     * @param event HttpSessionEvent object
     */
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        session.setAttribute("voted", new HashMap<Long, Boolean>());
    }

    /**
     * Remove user votes map from user session.
     *
     * @param event HttpSessionEvent object
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        session.removeAttribute("voted");
    }
}