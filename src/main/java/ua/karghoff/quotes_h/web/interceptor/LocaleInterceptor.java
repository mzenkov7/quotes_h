package ua.karghoff.quotes_h.web.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Add list of supported locales to requests.
 */
public class LocaleInterceptor extends HandlerInterceptorAdapter {
    private List<String> supportedLocales;

    public void setSupportedLocales(List<String> supportedLocales) {
        this.supportedLocales = supportedLocales;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("supportedLocales", supportedLocales);
        return super.preHandle(request, response, handler);
    }
}
