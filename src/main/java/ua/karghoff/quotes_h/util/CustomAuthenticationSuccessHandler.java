package ua.karghoff.quotes_h.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Custom handler for success authentication event. <br>
 * Add user entity to app user session after success login.
 */
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger logger = Logger.getLogger(CustomAuthenticationSuccessHandler.class);

    @Autowired
    UserService userService;
 
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = userService.getByName(username);

        session.setAttribute("user", user);

        if (logger.isInfoEnabled()) {
            logger.info("User with name '" + username + "' sign in.");
        }

        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect(request.getContextPath() + "/");
    }
}