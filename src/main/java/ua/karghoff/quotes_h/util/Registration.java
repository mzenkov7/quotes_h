package ua.karghoff.quotes_h.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ua.karghoff.quotes_h.entity.Role;
import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.service.UserService;
import ua.karghoff.quotes_h.service.exception.DuplicateEntryException;
import ua.karghoff.quotes_h.service.exception.FieldsValidateException;

/**
 * Util class for registration process. <br>
 * Check given params, if ok save to Database.
 */
@Component
public class Registration {

    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder encoder;

    public User register(String name, String password)
            throws FieldsValidateException {

        if ((name == null || password == null) ||
                (name.isEmpty()) || password.isEmpty()) {
            throw new FieldsValidateException();
        }

        User u = userService.getByName(name);
        if (u != null) {
            throw new DuplicateEntryException();
        }

        User user = new User();
        user.setPassword(encoder.encode(password));
        user.setName(name);
        user.setRole(Role.ROLE_USER);

        userService.insert(user);

        return userService.getByName(name);
    }
}
