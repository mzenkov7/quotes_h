package ua.karghoff.quotes_h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.karghoff.quotes_h.entity.Quote;

import java.util.List;

/**
 * Database repository for {@link Quote} entity. <br>
 * Provide basic CRUD operations with additional methods.
 */
@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {

    /**
     * Change rating for given quote. <br>
     * Change current rating, add/remove given diff value.
     *
     * @param id   quote unique id
     * @param diff rating difference to apply
     */
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Quote q set q.rating = q.rating + :diff WHERE q.id = :id")
    void changeRating(@Param("id") Long id, @Param("diff") int diff);

    /**
     * Get list with {@code count} random quotes.
     *
     * @param count number of expected quotes
     * @return {@code number} random quotes
     */
    @Query(value = "SELECT * FROM Quote ORDER BY RAND() LIMIT ?1", nativeQuery = true)
    List<Quote> getRandomQuotes(long count);
}